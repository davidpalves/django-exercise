from django.db import models

from django.contrib.auth.models import AbstractBaseUser

# Create your models here.
class User(AbstractBaseUser):
    name = models.CharField(max_length=255)
    enrollment_completed = models.BooleanField(default=False)
    student_id = models.BigAutoField(primary_key=True)

