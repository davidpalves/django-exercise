from django.test import TestCase
from .models import User

# Create your tests here.
class UserViewSetTests(TestCase):
    def setUp(self):
        User.objects.create(name='Test')

    def test_list_users(self):
        response = self.client.get('/users/')
        self.assertEqual(response.status_code, 200)

    def test_detail_user(self):
        response = self.client.get('/users/1/')
        self.assertEqual(response.status_code, 200)

    def test_detail_user_returns_not_found(self):
        response = self.client.get('/users/99/')
        self.assertEqual(response.status_code, 404)
